# Информация о проекте.
## Приложение "Task Manager"
Осуществляет вывод информации по введённой команде.

### Команды:
- help - доступные команды.
- about - информация о разработчике.
- version - версия приложения.

# Стек
- Java 8.
- IntelliJ IDEA.

# Аппаратное обеспечение.
Процессор: 
- Intel Core 2 Quad и выше.
- Amd Athlon 64 и выше. 

ОЗУ: 2гб.    

Графический память: 512 Мб.

Переферийные устройства: клавиатура, мышь.          
       
# Программное обеспечение.
- JDK 1.8.
- Windows 7.

# Сборка jar файла
``-cmf MANIFEST.MF task-manager-03.jar``
# Запуск приложения.
`` java -jar ./task-manager-03.jar command-1 command-2...``

![](https://drive.google.com/uc?export=view&id=1VgKfVtic135O6NWmC8CY4EaVWpY-n_Su)

![](https://drive.google.com/uc?export=view&id=1tTd1hmN9gln4w23Od8XJi-y4KddPJ5tG)

# Разработчики.
**Имя**: Александр Карамышев.

**Email**: sanja_19.96@mail.ru.

# Скриншоты.
 [https://drive.google.com/drive/folders/15C-BEvI1QTSxDJKTpf4vMGST8Q4IjaJu](https://drive.google.com/drive/folders/15C-BEvI1QTSxDJKTpf4vMGST8Q4IjaJu)