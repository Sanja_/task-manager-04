package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.constant.TerminalConst;

public class Application implements TerminalConst {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
    }

    private static void parsArgs(final String[] args) {
        if (args == null || args.length == 0){
            System.out.println("Error! Command absent.");
            return;
        }

        for (String arg : args) {
            chooseResponsCommand(arg);
        }
    }

    private static void chooseResponsCommand(String arg) {
        switch (arg) {
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            default:
                System.out.println("Error! Command not found.");
        }
    }

    private static void showVersion() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("\n [ABOUT]");
        System.out.println("NAME: Alexander Karamyshev");
        System.out.println("EMAIL: sanja_19.96@mail.ru");
    }

    private static void showHelp() {
        System.out.println("\n [HELP]");
        System.out.println("about - Show developer info.");
        System.out.println("version - Show version info.");
        System.out.println("help - Display terminal commands.");
    }
}
